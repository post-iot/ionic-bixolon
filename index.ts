import { Cordova, Plugin, IonicNativePlugin } from '@ionic-native/core';
import { Injectable } from '@angular/core';

/**
 * @name Bixolon
 * @description
 *
 * @usage
 * ```typescript
 * import { Bixolon } from '@ionic-native/bixolon';
 *
 *
 * constructor(private bixolon: Bixolon) { }
 *
 * ...
 *
 *
 * ```
 */
@Plugin({
  pluginName: 'Bixolon',
  plugin: 'com.bxl.service.cordova',
  pluginRef: 'bxl_service',
  repo: '',
  platforms: ['Android']
})
@Injectable()
export class Bixolon extends IonicNativePlugin {

    @Cordova()
    getClaimed(): Promise<any> { return; }

    @Cordova()
    getDeviceEnabled(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setDeviceEnabled(deviceEnabled: any): Promise<any> { return; }

    @Cordova()
    getOutputID(): Promise<any> { return; }


    @Cordova()
    getPowerState(): Promise<any> { return; }

    @Cordova()
    getState(): Promise<any> { return; }

    @Cordova()
    getDeviceServiceDescription(): Promise<any> { return; }

    @Cordova()
    getDeviceServiceVersion(): Promise<any> { return; }

    @Cordova()
    getPhysicalDeviceDescription(): Promise<any> { return; }

    @Cordova()
    getPhysicalDeviceName(): Promise<any> { return; }

    @Cordova()
    getCapRecNearEndSensor(): Promise<any> { return; }

    @Cordova()
    getCapRecPapercut(): Promise<any> { return; }

    @Cordova()
    getCapRecMarkFeed(): Promise<any> { return; }

    @Cordova()
    getAsyncMode(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setAsyncMode(asyncMode: any): Promise<any> { return; }

    @Cordova()
    getCharacterSet(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setCharacterSet(characterSet?: any): Promise<any> { return; }

    @Cordova()
    getPageModeArea(): Promise<any> { return; }

    @Cordova()
    getPageModeHorizontalPosition(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setPageModeHorizontalPosition(position?: any): Promise<any> { return; }

    @Cordova()
    getPageModePrintArea(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setPageModePrintArea(area?: any): Promise<any> { return; }

    @Cordova()
    getPageModePrintDirection(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setPageModePrintDirection(direction?: any): Promise<any> { return; }

    @Cordova()
    getPageModeVerticalPosition(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    setPageModeVerticalPosition(position?: any): Promise<any> { return; }


    @Cordova()
    getRecEmpty(): Promise<any> { return; }


    @Cordova()
    getRecNearEnd(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    open(logicalDeviceName?: any): Promise<any> { return; }

    @Cordova()
    close(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    claim(timeout?: any): Promise<any> { return; }

    @Cordova()
    release(): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    checkHealth(level?: any): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    cutPaper(percentage?: any): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    markFeed(type?: any): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    pageModePrint(control?: any): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    printBarCode(
        station?: any,
        data?: any,
        symbology?: any,
        height?: any,
        width?: any,
        alignment?: any,
        textPosition?: any
    ): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    printBitmap(
        station?: any,
        fileName?: any,
        width?: any,
        alignment?: any
    ): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    printBitmapWithURL(
        station?: any,
        imageURL?: any,
        width?: any,
        alignment?: any
    ): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    printBitmapWithBase64(
        station?: any,
        base64Data?: any,
        width?: any,
        alignment?: any
    ): Promise<any> { return; }


    @Cordova({
        callbackOrder: 'reverse'
    })
    printNormal(
        station?: any,
        data?: any
    ): Promise<any> { return; }


    @Cordova({
        callbackOrder: 'reverse'
    })
    transactionPrint(
        station?: any,
        control?: any
    ): Promise<any> { return; }

    @Cordova({
        callbackOrder: 'reverse'
    })
    addEntry(
        productName?: any,
        ifType?: any,
        bluetoothAddress?: any
    ): Promise<any> { return; }


    @Cordova()
    startActivity(): Promise<any> { return; }

    @Cordova()
    getPariedDevice(): Promise<any> { return; }

}
